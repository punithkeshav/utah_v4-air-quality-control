/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.Air_Quality_Monitoring_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "FR2_Capture_Emmissions_Measurement_OptionalScenario",
        createNewBrowserInstance = false
)

public class FR2_CaptureEmmissionsMeasurement_OS extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_CaptureEmmissionsMeasurement_OS()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!uploadDocument())
        {
            return narrator.testFailed("Uploaded Document Failed Due To :" + error);
        }
        return narrator.finalizeTest("Successfully Uploaded Document");
    }

    public boolean uploadDocument()
    {

        //Click upload document
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.linkToADocument()))
        {
            error = "Failed to wait for Supporting documents 'Upload document' link.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.linkToADocument()))
        {
            error = "Failed to click on Supporting documents 'Upload document' link.";
            return false;
        }

        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully clicked Supporting documents 'Upload Hyperlink box'.");

        //switch to new window
        if (!SeleniumDriverInstance.switchToTabOrWindow())
        {
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.LinkURL()))
        {
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.LinkURL(), getData("Document Link")))
        {
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.urlTitle()))
        {
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.urlTitle(), getData("Title")))
        {
            error = "Failed to enter '" + getData("Title") + "' into ' Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.urlAddButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.urlAddButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("URL Title") + "' document using '" + getData("Document Link") + "' Link.");

        if (!SeleniumDriverInstance.switchToFrameByXpath(Air_Quality_Monitoring_PageObjects.iframe()))
        {
            error = "Failed to switch to frame ";
            return false;

        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");

        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_Parameter_panel()))
        {
            error = "Failed to wait for 'Parameter Readings' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CEM_Parameter_panel()))
        {
            error = "Failed to click on 'Parameter Readings' tab.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully navigated to 'Parameter Readings' tab.");

//        for (int i = 1; i < 2; i++)
//        {
//            //Measurement
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_A519A7A7-3788-4E44-A761-AB93971C4477']//input[@type='number'])[1]"))
//            {
//                error = "Failed to wait for Measurement field";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_A519A7A7-3788-4E44-A761-AB93971C4477']//input[@type='number'])[1]", getData("Measurement")))
//            {
//                error = "Failed to enter text in Measurement field";
//                return false;
//            }
//
//            narrator.stepPassed("Measurement 1:" + getData("Measurement"));
//
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_A519A7A7-3788-4E44-A761-AB93971C4477']//input[@type='number'])[2]", getData("Measurement")))
//            {
//                error = "Failed to enter text in Measurement field";
//                return false;
//            }
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_A519A7A7-3788-4E44-A761-AB93971C4477']//input[@type='number'])[2]", getData("Measurement")))
//            {
//                error = "Failed to enter text in Measurement field";
//                return false;
//            }
//
//            narrator.stepPassed("Measurement 2:" + getData("Measurement"));
//
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_A519A7A7-3788-4E44-A761-AB93971C4477']//input[@type='number'])[3]", getData("Measurement")))
//            {
//                error = "Failed to enter text in Measurement field";
//                return false;
//            }
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_A519A7A7-3788-4E44-A761-AB93971C4477']//input[@type='number'])[3]", getData("Measurement")))
//            {
//                error = "Failed to enter text in Measurement field";
//                return false;
//            }
//
//            narrator.stepPassed("Measurement 3:" + getData("Measurement"));
//        }
//
//        for (int i = 1; i < 2; i++)
//        {
//            //Unit
//            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.UnitDropdown(i)))
//            {
//                error = "Failed to wait for Unit Dropdown";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.UnitDropdown(i)))
//            {
//                error = "Failed to click on Unit Dropdown";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Unit"))))
//            {
//                error = "Failed to wait for Unit down option : " + getData("Unit");
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Unit"))))
//            {
//                error = "Failed to click Unit drop down option : " + getData("Unit");
//                return false;
//            }
//            narrator.stepPassed("Unit 1 :" + getData("Unit"));
//
//            //Unit 2
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_262495D5-3D67-461A-AE55-2BB7CE6BD497']//ul)[2]"))
//            {
//                error = "Failed to wait for Unit Dropdown";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath("(//div[@id='control_262495D5-3D67-461A-AE55-2BB7CE6BD497']//ul)[2]"))
//            {
//                error = "Failed to click on Unit Dropdown";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Unit"))))
//            {
//                error = "Failed to wait for Unit down option : " + getData("Unit");
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Unit"))))
//            {
//                error = "Failed to click Unit drop down option : " + getData("Unit");
//                return false;
//            }
//            narrator.stepPassed("Unit 2 :" + getData("Unit"));
//
//            //Unit 3
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_262495D5-3D67-461A-AE55-2BB7CE6BD497']//ul)[3]"))
//            {
//                error = "Failed to wait for Unit Dropdown";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath("(//div[@id='control_262495D5-3D67-461A-AE55-2BB7CE6BD497']//ul)[3]"))
//            {
//                error = "Failed to click on Unit Dropdown";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Unit"))))
//            {
//                error = "Failed to wait for Unit down option : " + getData("Unit");
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Unit"))))
//            {
//                error = "Failed to click Unit drop down option : " + getData("Unit");
//                return false;
//            }
//            narrator.stepPassed("Unit 3 :" + getData("Unit"));
//        }
//
//        for (int i = 1; i < 2; i++)
//        {
//
//            //Sample date 1  
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[1]"))
//            {
//                error = "Failed to wait for Sample date";
//                return false;
//            }
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[1]", startDate))
//            {
//                error = "Failed to enter Sample date";
//                return false;
//            }
//
//            //Sample date 2
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[2]"))
//            {
//                error = "Failed to wait for Sample date";
//                return false;
//            }
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[2]", startDate))
//            {
//                error = "Failed to enter Sample date";
//                return false;
//            }
//
//            //Sample date 3  
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[3]"))
//            {
//                error = "Failed to wait for Sample date";
//                return false;
//            }
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[3]", startDate))
//            {
//                error = "Failed to enter Sample date";
//                return false;
//            }
//        }
//
//        for (int i = 1; i < 2; i++)
//        {
//            //Comments
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[1]"))
//            {
//                error = "Failed to wait for Comments field";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[1]", testData.getData("Comments")))
//            {
//                error = "Failed to enter text in Comments field";
//                return false;
//            }
//            narrator.stepPassed("Comments 1 :" + getData("Comments"));
//
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[2]"))
//            {
//                error = "Failed to wait for Comments field";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[2]", testData.getData("Comments")))
//            {
//                error = "Failed to enter text in Comments field";
//                return false;
//            }
//            narrator.stepPassed("Comments 2 :" + getData("Comments"));
//
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[3]"))
//            {
//                error = "Failed to wait for Comments field";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[3]", testData.getData("Comments")))
//            {
//                error = "Failed to enter text in Comments field";
//                return false;
//            }
//            narrator.stepPassed("Comments 3 :" + getData("Comments"));
//
//        }//end of for loop

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementNoLongerPresentByXpath(Air_Quality_Monitoring_PageObjects.SavingMask()))
//        {
//            error = "Failed to wait for saving mask to be no longer present";
//            return false;
//
//        }
        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup()))
        {
            if (!SeleniumDriverInstance.waitForElementNoLongerPresentByXpath(Air_Quality_Monitoring_PageObjects.SavingMask()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.getActionRecord());
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Successfully Saved Record :" + acionRecord);

        return true;
    }

}
