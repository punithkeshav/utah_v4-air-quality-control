/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.Air_Quality_Monitoring_PageObjects;

/**
 *
 * @author SMABE
 */
@KeywordAnnotation(
        Keyword = "FR5-Edit Air Quality Monitoring",
        createNewBrowserInstance = false
)
public class FR5_EditAirQuality_Monitoring extends BaseClass
{

    String error = "";

    public FR5_EditAirQuality_Monitoring()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!SearchRecord())
        {
            return narrator.testFailed("Failed To Edit Record Due To:" + error);
        }

        return narrator.finalizeTest("Successfully Edited Waste Management Record");
    }

    public boolean SearchRecord() throws InterruptedException
    {

        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monthTab())){
            error = "Failed to wait for 'Month' tab.";
            return false;
        }

        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.getActionRecord());
        String[] array = acionRecord.split("#");

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CloseCurrentModule()))
        {
            error = "Failed to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to wait for the cross to close current module";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CloseCurrentModule2()))
        {
            error = "Failed to close current module";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully closed current module");

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.airQualityMonitoringTab()))
        {
            error = "Failed To Wait For Waste Monitoring Tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.airQualityMonitoringTab()))
        {
            error = "Failed Click Waste Monitoring Tab";
            return false;
        }
        pause(3000);

        narrator.stepPassedWithScreenShot("Waste Monitoring Tab Clicked Successfully");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.addButton()))
        {
            error = "Failed to locate Waste Management add button";
            return false;
        }

        //contains
        if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.ContainsTextBox(), array[1]))
        {
            error = "Failed to enter contains to search";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.SearchButton()))
        {
            error = "Failed Click Search Button";
            return false;
        }
        pause(3000);
        narrator.stepPassedWithScreenShot("Searched For Record :" + array[1]);

        //Record
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Record(array[1])))
        {
            error = "Failed to wait for record";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Record(array[1])))
        {
            error = "Failed click record";
            return false;
        }

        narrator.stepPassedWithScreenShot("Record Found and clicked record : " + array[1]);

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementsPanel()))
        {
            error = "Failed to locate Measurements panel";
            return false;
        }

        pause(6000);
         //Sample taken by
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.SampleTakenByDropDown()))
        {
            error = "Failed to click Sample taken by drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Sample taken by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch(), getData("Sample taken by")))
        {
            error = "Failed to enter Sample taken by :" + getData("Sample taken by");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(Air_Quality_Monitoring_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text(getData("Sample taken by"))))
        {
            error = "Failed to wait for Sample taken by drop down option : " + getData("Sample taken by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text(getData("Sample taken by"))))
        {
            error = "Failed to click Sample taken by drop down option : " + getData("Sample taken by");
            return false;
        }

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.getActionRecord());
        narrator.stepPassed("Record number :" + acionRecord);

        narrator.stepPassedWithScreenShot("Recored Edited");
        pause(5000);

        return true;
    }

}
