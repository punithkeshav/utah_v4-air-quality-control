/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.Air_Quality_Monitoring_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author RNagel
 */
@KeywordAnnotation(
        Keyword = "Capture Air Quality Monitoring - Main Scenario",
        createNewBrowserInstance = false
)

public class FR1_CaptureAirQualityMonitoring_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR1_CaptureAirQualityMonitoring_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToAirQualityMonitoring())
        {
            return narrator.testFailed("Failed To Capture Emissions Measurement Due To :" + error);
        }
        if (!enterDetails())
        {
            return narrator.testFailed("Failed To Capture Emissions Measurement Due To :" + error);
        }
        return narrator.finalizeTest("Successfully Capture Emissions Measurement");
    }

    public boolean navigateToAirQualityMonitoring()
    {
        pause(2000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.EnvironmentalSustainabilityTab()))
        {
            error = "Failed to locate the module : Environmental Sustainability Tab";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.EnvironmentalSustainabilityTab()))
        {
            error = "Failed to navigate to the module : Environmental SustainabilityPageName";
            return false;
        }
        narrator.stepPassedWithScreenShot("Environmental Sustainability Tab Clicked Successfully");

        pause(4000);

        //Navigate to Air Quality Monitoring
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.airQualityMonitoringTab()))
        {
            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.EnvironmentalSustainabilityTab()))
            {
                error = "Failed to wait for 'Air Quality Monitoring' tab.";
                return false;
            }
        }
        pause(3000);
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.airQualityMonitoringTab()))
        {
            error = "Failed to click on 'Air Quality Monitoring' tab.";
            return false;
        }

        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Air Quality Monitoring' tab.");

        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.addButton()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.addButton()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }

        pause(3000);
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    //Enter data
    public boolean enterDetails()
    {

        pause(5000);

        //Process flow 2 
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.processFlow2()))
        {
            error = "Failed to wait for 'Process Flow' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.processFlow2()))
        {
            error = "Failed to click on 'Process Flow' tab.";
            return false;
        }
        //Business Unit Dropdown

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to locate Business Unit Dropdown";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.businessUnitDropdown()))
        {
            error = "Failed to click on Business Unit Dropdown";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for Business Unit text box.";
//            return false;
//        }
//
//        if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch2(), getData("Business unit option")))
//        {
//            error = "Failed to wait for Business Unit :" + getData("Business unit option");
//            return false;
//        }
//        if (!SeleniumDriverInstance.pressEnter_2(Air_Quality_Monitoring_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to press enter";
//            return false;
//        }
        pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to wait to expand Business Unit";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.businessUnitOption1(getData("Business unit"))))
        {
            error = "Failed to expand Business Unit";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to wait for Business Unit:" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.businessUnitOption1(getData("Business unit 1"))))
        {
            error = "Failed to click Business Unit Option drop down :" + getData("Business unit 1");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to wait for Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.businessUnitOption1(getData("Business unit 2"))))
        {
            error = "Failed to click  Business Unit Option drop down :" + getData("Business unit 2");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 2");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.businessUnitOption1(getData("Business unit 3"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 3");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.businessUnitOption1(getData("Business unit 4"))))
        {
            error = "Failed to click Entity Option drop down :" + getData("Business unit 4");
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down :" + getData("Business unit 5");
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.businessUnitOption1(getData("Business unit 5"))))
        {
            error = "Failed to wait for Entity Option drop down:" + getData("Business unit 5");
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to wait for Entity drop down option : " + getData("Business unit option");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text(getData("Business unit option"))))
        {
            error = "Failed to click Entity drop down option : " + getData("Business unit option");
            return false;
        }

        narrator.stepPassedWithScreenShot("Business unit option  :" + getData("Business unit option"));

        pause(3000);
        //Air quality type
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.airQualityType()))
        {
            error = "Failed to wait for 'Air Quality Type' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.airQualityType()))
        {
            error = "Failed to click on 'Air Quality Type' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.airQualityTypeSelect(getData("Air Quality Type"))))
        {
            error = "Failed to wait for Air Quality Type option: '" + getData("Air Quality Type") + "'.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.airQualityTypeSelect(getData("Air Quality Type"))))
        {
            error = "Failed to select Air Quality Type option: '" + getData("Air Quality Type") + "'.";
            return false;
        }
        String airQualityType = getData("Air Quality Type");
        switch (airQualityType)
        {
            case "Emission":
                //Monitoring poin
                narrator.stepPassedWithScreenShot("Air Quality Type : '" + airQualityType + "'.");

                if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monitoringPointTab()))
                {
                    error = "Failed to wait for 'Monitoring point' tab.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.monitoringPointTab()))
                {
                    error = "Failed to click on 'Monitoring point' tab.";
                    return false;
                }

                if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monitoringPointOption(getData("Monitoring Point"))))
                {
                    error = "Failed to wait for '" + getData("Monitoring Point") + "' option.";
                    return false;
                }
                if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.monitoringPointOption(getData("Monitoring Point"))))
                {
                    error = "Failed to click on '" + getData("Monitoring Point") + "' option.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Monitoring Point : '" + getData("Monitoring Point") + "'.");
                break;
            default:
                narrator.stepPassedWithScreenShot("Air Quality Type : '" + airQualityType + "'.");
        }

        //Month / Year
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monthTab()))
        {
            error = "Failed to wait for 'Month' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.monthTab()))
        {
            error = "Failed to click on 'Month' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monthOption(getData("Month"))))
        {
            error = "Failed to wait for '" + getData("Month") + "' option.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.monthOption(getData("Month"))))
        {
            error = "Failed to click on '" + getData("Month") + "' option.";
            return false;
        }

        //Year
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.yearTab()))
        {
            error = "Failed to wait for 'Year' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.yearTab()))
        {
            error = "Failed to click on 'Year' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.yearOption(getData("Year"))))
        {
            error = "Failed to wait for '" + getData("Year") + "' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.yearOption(getData("Year"))))
        {
            error = "Failed to click on '" + getData("Year") + "' tab.";
            return false;
        }

        //Sample taken by
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.SampleTakenByDropDown()))
        {
            error = "Failed to click Sample taken by drop down";
            return false;
        }

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Sample taken by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch(), getData("Sample taken by")))
        {
            error = "Failed to enter Sample taken by :" + getData("Sample taken by");
            return false;
        }
        if (!SeleniumDriverInstance.pressEnter_2(Air_Quality_Monitoring_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text(getData("Sample taken by"))))
        {
            error = "Failed to wait for Sample taken by drop down option : " + getData("Sample taken by");
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text(getData("Sample taken by"))))
        {
            error = "Failed to click Sample taken by drop down option : " + getData("Sample taken by");
            return false;
        }

//         //Team
//        
//        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.TeamNameDropDown()))
//        {
//            error = "Failed to wait for Team Name drop down :";
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.TeamNameDropDown()))
//        {
//            error = "Failed to click Team Name drop down :";
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch2()))
//        {
//            error = "Failed to wait for Team Name text box";
//            return false;
//        }
//        if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch2(), getData("Team Name")))
//        {
//            error = "Failed to enter  Team Name :" + getData("Team Name");
//            return false;
//        }
//        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text(getData("Team Name"))))
//        {
//            error = "Failed to wait for Team Name drop down option : " + getData("Team Name");
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text(getData("Team Name"))))
//        {
//            error = "Failed to click Team Name drop down option : " + getData("Team Name");
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Team Name :" + getData("Team Name"));
//        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

//        if (!SeleniumDriverInstance.waitForElementNoLongerPresentByXpath(Air_Quality_Monitoring_PageObjects.SavingMask()))
//        {
//            error = "Failed to wait for saving mask to be no longer present";
//            return false;
//
//        }
        //pause(2000);
        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup()))
        {
            if (!SeleniumDriverInstance.waitForElementNoLongerPresentByXpath(Air_Quality_Monitoring_PageObjects.SavingMask()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.getActionRecord());
        narrator.stepPassed("Record number :" + acionRecord);

        if (getData("Link to site obligation").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.LinkToSite()))
            {
                error = "Failed To Wait For Link To Site Obligation";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.LinkToSite()))
            {
                error = "Failed To Click On Link To Site Obligation Obligation";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.LinkToSiteDropDown()))
            {
                error = "Failed To Wait For Link To Site Obligation Drop Down ";
                return false;
            }

        }

        if (getData("Link to projects").equalsIgnoreCase("True"))
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.LinkToProjects()))
            {
                error = "Failed To Wait For Link To Projects";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.LinkToProjects()))
            {
                error = "Failed To Click On Link To Projects";
                return false;
            }
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.LinkToProjectsDropDown()))
            {
                error = "Failed To Wait For Link To Projects Drop Down";
                return false;
            }
        }

        if (getData("Upload Document").equalsIgnoreCase("True"))
        {
            //Upload a link
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.linkADoc_button()))
            {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }

            if (!SeleniumDriverInstance.scrollToElement(Air_Quality_Monitoring_PageObjects.linkADoc_button()))
            {
                error = "Failed to scroll to 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.linkADoc_button()))
            {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }

            narrator.stepPassed("Successfully click 'Link a document' button.");

            if (!SeleniumDriverInstance.switchToTabOrWindow())
            {
                error = "Failed to switch tab.";
                return false;
            }

            //URL Input
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.urlInput_TextAreaxpath()))
            {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.urlInput_TextAreaxpath(), getData("Document Link")))
            {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }

            //Title Input
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.tile_TextAreaxpath()))
            {
                error = "Failed to wait for 'Link a document' button.";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.tile_TextAreaxpath(), getData("Title")))
            {
                error = "Failed to click on 'Link a document' button.";
                return false;
            }

            //Add button
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.linkADoc_Add_buttonxpath()))
            {
                error = "Failed to wait for 'Add' button.";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.linkADoc_Add_buttonxpath()))
            {
                error = "Failed to click on 'Add' button.";
                return false;
            }

            narrator.stepPassed("Successfully uploaded a Supporting Document.");

            if (!SeleniumDriverInstance.switchToDefaultContent())
            {
                error = "Failed to switch tab.";
                return false;
            }

            //iFrame
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.iframe()))
            {
                error = "Failed to switch to frame.";
                return false;
            }
            if (!SeleniumDriverInstance.switchToFrameByXpath(Air_Quality_Monitoring_PageObjects.iframe()))
            {
                error = "Failed to switch to frame.";
                return false;
            }

            //Save button
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
            {
                error = "Failed to locate Save button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
            {
                error = "Failed to click Save button";
                return false;
            }
            //pause(3000);

            //Check if the record has been Saved
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }

            saved = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            //Getting the action No
            acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.getActionRecord());
            narrator.stepPassed("Record number :" + acionRecord);
        }

        return true;
    }

}
