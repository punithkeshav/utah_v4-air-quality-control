/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses;


import KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.Air_Quality_Monitoring_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */

@KeywordAnnotation(
        Keyword = "Capture Dust Measurements MainScenario",
        createNewBrowserInstance = false
)

public class FR3_CaptureDustMeasurements_MS extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR3_CaptureDustMeasurements_MS()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToMeasurements())
        {
            return narrator.testFailed("Failed To Capture Dust Measurements Due To : " + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed To Capture Dust Measurements Due To :" + error);
        }
        return narrator.finalizeTest("Successfully Captureed Dust Measurements");
    }

    public boolean navigateToMeasurements(){
        //Navigate to Measurement Tab
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementTab())){
            error = "Failed to wait for 'Measurements' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.measurement())){
            error = "Failed to click on 'Measurements' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Measurements' panel.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementsAddButton())){
            error = "Failed to wait for Measurements Add button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.measurementsAddButton())){
            error = "Failed to click on Measurements Add button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked Measurements Add button.");
        
        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
  
        
        //Monitoring point
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementMonitoringTab())){
            error = "Failed to wait for 'Monitoring point' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.measurementMonitoringTab())){
            error = "Failed to click on 'Monitoring point' tab.";
            return false;
        }
        
        
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to wait for text box";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch2(), getData("Monitoring Point1")))
        {
            error = "Failed to enter Monitoring Point option :" + getData("Monitoring Point1");
            return false;
        }
       if (!SeleniumDriverInstance.pressEnter_2(Air_Quality_Monitoring_PageObjects.TypeSearch2()))
        {
            error = "Failed to press enter";
            return false;
        }
  
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text4(getData("Monitoring Point1"))))
        {
            error = "Failed to wait for Monitoring Point drop down option : " + getData("Monitoring Point1");
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text4(getData("Monitoring Point1"))))
        {
            error = "Failed to click  Monitoring Point drop down option : " + getData("Monitoring Point1");
            return false;
        }

        pause(2000);

        //Measurement
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementField())){
            error = "Failed to wait for Measurement text field.";
            return false;
        }
        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.measurementField(), getData("MeasurementNo"))){
            error = "Failed to enter Measurement :" + getData("MeasurementNo");
            return false;
        }
        narrator.stepPassedWithScreenShot("Measurement : '#" + getData("MeasurementNo") + "'.");
        
        //Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.unitTab())){
            error = "Failed to wait for 'Unit' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.unitTab())){
            error = "Failed to click on 'Unit' tab.";
            return false;
        }
        
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.unitOption(getData("Unit")))){
            error = "Failed to wait for '" + getData("Unit") + "' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.unitOption(getData("Unit")))){
            error = "Failed to click on '" + getData("Unit") + "' tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Unit: '" + getData("Unit") + "'.");
        
        //Save button
         if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
            {
                error = "Failed to locate Save button";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
            {
                error = "Failed to click Save button";
                return false;
            }

            //Check if the record has been Saved
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }

            String saved = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup());

            if (saved.equals("Record saved"))
            {
                narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
            } else
            {
                if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.failed()))
                {
                    error = "Failed to wait for error message.";
                    return false;
                }

                String failed = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.failed());

                if (failed.equals("ERROR: Record could not be saved"))
                {
                    error = "Failed to save record.";
                    return false;
                }
            }

            //Getting the action No

        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.dustMeasureRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    

        
        return true;
    }

}
