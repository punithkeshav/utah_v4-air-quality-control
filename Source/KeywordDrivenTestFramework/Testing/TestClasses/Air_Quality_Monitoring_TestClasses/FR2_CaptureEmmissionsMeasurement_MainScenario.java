/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.Air_Quality_Monitoring_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Emissions Measurement - Main Scenario",
        createNewBrowserInstance = false
)

public class FR2_CaptureEmmissionsMeasurement_MainScenario extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR2_CaptureEmmissionsMeasurement_MainScenario()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("dd-MM-YYYY").format(cal.getTime());
    }

    public TestResult executeTest()
    {
        if (!navigateToParameterRedings())
        {
            return narrator.testFailed("Failed To Capture Emissions Measurement Due To : " + error);
        }
        if (!enterDetails())
        {
            return narrator.testFailed("Failed To Capture Emissions Measurement Due To : " + error);
        }
        return narrator.finalizeTest("Successfully Capture Emissions Measurement");
    }

    public boolean navigateToParameterRedings()
    {
        //Navigate to Parameter Readings
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_Parameter_panel()))
        {
            error = "Failed to wait for 'Parameter Readings' tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CEM_Parameter_panel()))
        {
            error = "Failed to click on 'Parameter Readings' tab.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully navigated to 'Parameter Readings' tab.");

        //Add Button
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.CEM_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.CEM_Add()))
        {
            error = "Failed to click 'Add' button.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }

    //Enter data
    public boolean enterDetails()
    {

        //Save to continue
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        String total = Air_Quality_Monitoring_PageObjects.EmissionMeasurementsDeleteButtons();
        List<WebElement> elementList = SeleniumDriverInstance.Driver.findElements(By.xpath(Air_Quality_Monitoring_PageObjects.MeasurementTextBox()));
        List<WebElement> deletebutons = SeleniumDriverInstance.Driver.findElements(By.xpath(Air_Quality_Monitoring_PageObjects.Deletebutons()));
        int TableSize = elementList.size();

        for (int i = 1; i < 2; i++)
        {

            //Parameter
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.ParameterDropdown()))
            {
                error = "Failed to wait for Parameter Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.ParameterDropdown()))
            {
                error = "Failed to click on Parameter Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch2()))
            {
                error = "Failed to wait for Parameter text box.";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch2(), getData("Parameter")))
            {
                error = "Failed to enter Parameter :" + getData("Parameter");
                return false;
            }
            if (!SeleniumDriverInstance.pressEnter_2(Air_Quality_Monitoring_PageObjects.TypeSearch2()))
            {
                error = "Failed to press enter";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Parameter"))))
            {
                error = "Failed to wait for Parameter down option : " + getData("Parameter");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Parameter"))))
            {
                error = "Failed to click Parameter drop down option : " + getData("Parameter");
                return false;
            }

            narrator.stepPassed("Parameter :" + getData("Parameter"));

            //Measurement
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.measurementTextbox(i)))
            {
                error = "Failed to wait for Measurement field";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_A519A7A7-3788-4E44-A761-AB93971C4477']//input[@type='number'])[1]", getData("Measurement")))
            {
                error = "Failed to enter text in Measurement field";
                return false;
            }
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_A519A7A7-3788-4E44-A761-AB93971C4477']//input[@type='number'])[2]", getData("Measurement")))
//            {
//                error = "Failed to enter text in Measurement field";
//                return false;
//            }
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_A519A7A7-3788-4E44-A761-AB93971C4477']//input[@type='number'])[3]", getData("Measurement")))
//            {
//                error = "Failed to enter text in Measurement field";
//                return false;
//            }

            narrator.stepPassed("Measurement :" + getData("Measurement"));
        }

        for (int i = 1; i < 2; i++)
        {
            //Unit
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.UnitDropdown(i)))
            {
                error = "Failed to wait for Unit Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.UnitDropdown(i)))
            {
                error = "Failed to click on Unit Dropdown";
                return false;
            }

            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Unit"))))
            {
                error = "Failed to wait for Unit down option : " + getData("Unit");
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Unit"))))
            {
                error = "Failed to click Unit drop down option : " + getData("Unit");
                return false;
            }
            narrator.stepPassed("Unit 1 :" + getData("Unit"));

//            //Unit 2
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_262495D5-3D67-461A-AE55-2BB7CE6BD497']//ul)[2]"))
//            {
//                error = "Failed to wait for Unit Dropdown";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath("(//div[@id='control_262495D5-3D67-461A-AE55-2BB7CE6BD497']//ul)[2]"))
//            {
//                error = "Failed to click on Unit Dropdown";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Unit"))))
//            {
//                error = "Failed to wait for Unit down option : " + getData("Unit");
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Unit"))))
//            {
//                error = "Failed to click Unit drop down option : " + getData("Unit");
//                return false;
//            }
//            narrator.stepPassed("Unit 2 :" + getData("Unit"));
//
//            //Unit 3
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_262495D5-3D67-461A-AE55-2BB7CE6BD497']//ul)[3]"))
//            {
//                error = "Failed to wait for Unit Dropdown";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.clickElementbyXpath("(//div[@id='control_262495D5-3D67-461A-AE55-2BB7CE6BD497']//ul)[3]"))
//            {
//                error = "Failed to click on Unit Dropdown";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Unit"))))
//            {
//                error = "Failed to wait for Unit down option : " + getData("Unit");
//                return false;
//            }
//            if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Unit"))))
//            {
//                error = "Failed to click Unit drop down option : " + getData("Unit");
//                return false;
//            }
//            narrator.stepPassed("Unit 3 :" + getData("Unit"));
        }

        for (int i = 1; i < 2; i++)
        {

            //Sample date 1  
            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[1]"))
            {
                error = "Failed to wait for Sample date";
                return false;
            }
            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[1]", startDate))
            {
                error = "Failed to enter Sample date";
                return false;
            }

//            //Sample date 2
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[2]"))
//            {
//                error = "Failed to wait for Sample date";
//                return false;
//            }
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[2]", startDate))
//            {
//                error = "Failed to enter Sample date";
//                return false;
//            }
//
//            //Sample date 3  
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[3]"))
//            {
//                error = "Failed to wait for Sample date";
//                return false;
//            }
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[3]", startDate))
//            {
//                error = "Failed to enter Sample date";
//                return false;
//            }
        }

        for (int i = 1; i < 2; i++)
        {
            //Comments
            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[1]"))
            {
                error = "Failed to wait for Comments field";
                return false;
            }

            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[1]", testData.getData("Comments")))
            {
                error = "Failed to enter text in Comments field";
                return false;
            }
            narrator.stepPassed("Comments 1 :" + getData("Comments"));

//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[2]"))
//            {
//                error = "Failed to wait for Comments field";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[2]", testData.getData("Comments")))
//            {
//                error = "Failed to enter text in Comments field";
//                return false;
//            }
//            narrator.stepPassed("Comments 2 :" + getData("Comments"));
//
//            if (!SeleniumDriverInstance.waitForElementByXpath("(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[3]"))
//            {
//                error = "Failed to wait for Comments field";
//                return false;
//            }
//
//            if (!SeleniumDriverInstance.enterTextByXpath("(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[3]", testData.getData("Comments")))
//            {
//                error = "Failed to enter text in Comments field";
//                return false;
//            }
//            narrator.stepPassed("Comments 3 :" + getData("Comments"));
        }//end of for loop

        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup()))
        {
            if (!SeleniumDriverInstance.waitForElementNoLongerPresentByXpath(Air_Quality_Monitoring_PageObjects.SavingMask()))
            {
                error = "Failed to wait for 'Record Saved' popup.";
                return false;
            }
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }

        //Getting the action No
        String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.getActionRecord());
        narrator.stepPassed("Record number :" + acionRecord);
        narrator.stepPassedWithScreenShot("Record saved :" + acionRecord);

        return true;
    }

}
