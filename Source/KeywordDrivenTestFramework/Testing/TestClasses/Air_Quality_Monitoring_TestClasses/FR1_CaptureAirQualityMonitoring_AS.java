/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses;


import KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.Air_Quality_Monitoring_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author RNagel
 */

@KeywordAnnotation(
        Keyword = "Air Quality Monitoring - Alternate Scenario",
        createNewBrowserInstance = false
)

public class FR1_CaptureAirQualityMonitoring_AS extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;
    
    public FR1_CaptureAirQualityMonitoring_AS()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToAirQualityMonitoring())
        {
            return narrator.testFailed("Failed To Capture Air Quality Monitoring Due To :" + error);
        }
        if(!enterDetails()){
            return narrator.testFailed("Failed To Capture Air Quality Monitoring Due To : " + error);
        }
        return narrator.finalizeTest("Successfully navigated ");
    }

    public boolean navigateToAirQualityMonitoring(){
        //Navigate to Tailings Management
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.tailingManagementTab())){
            error = "Failed to wait for 'Tailings Management' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.tailingManagementTab())){
            error = "Failed to click on 'Tailings Management' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Tailings Management' tab.");
        

//        //Navigate to Tailings Management Monitoring
//        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.tailingManagementMonitoringTab())){
//            error = "Failed to wait for 'Tailings Management Monitoring' tab.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.tailingManagementMonitoringTab())){
//            error = "Failed to click on 'Tailings Management Monitoring' tab.";
//            return false;
//        }
//        SeleniumDriverInstance.pause(2000);
//        narrator.stepPassedWithScreenShot("Successfully navigated to 'Tailings Management Monitoring' tab.");
//        

        //Navigate to Air Quality Monitoring
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.airQualityMonitoringTab())){
            error = "Failed to wait for 'Air Quality Monitoring' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.airQualityMonitoringTab())){
            error = "Failed to click on 'Air Quality Monitoring' tab.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully navigated to 'Air Quality Monitoring' tab.");
        
        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.addButton())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.addButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully click 'Add' button.");

        return true;
    }
    
    //Enter data
    public boolean enterDetails(){
         //Process flow 2 
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.processFlow2())){
            error = "Failed to wait for 'Process Flow' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.processFlow2())){
            error = "Failed to click on 'Process Flow' tab.";
            return false;
        }
        
        //Business Unit
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.businessUnitTab())){
            error = "Failed to wait for 'Business Unit' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.businessUnitTab())){
            error = "Failed to click 'Business Unit' dropdown.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.businessUnitSelect(getData("Business Unit")))){
            error = "Failed to wait for Business Unit option: '" + getData("Business Unit") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.businessUnitSelect(getData("Business Unit")))){
            error = "Failed to select Business Unit option: '" + getData("Business Unit") + "'.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Business unit : '" + getData("Business Unit") + "'.");
        
        //Air quality type
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.airQualityType())){
            error = "Failed to wait for 'Air Quality Type' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.airQualityType())){
            error = "Failed to click on 'Air Quality Type' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.airQualityTypeSelect(getData("Air Quality Type")))){
            error = "Failed to wait for Air Quality Type option: '" + getData("Air Quality Type") + "'.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.airQualityTypeSelect(getData("Air Quality Type")))){
            error = "Failed to select Air Quality Type option: '" + getData("Air Quality Type") + "'.";
            return false;
        }
        String airQualityType = getData("Air Quality Type");
        switch(airQualityType){
            case "Emission":
                //Monitoring point
                narrator.stepPassedWithScreenShot("Air Quality Type : '" + airQualityType + "'.");
                
                if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monitoringPointTab())){
                    error = "Failed to wait for 'Monitoring point' tab.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.monitoringPointTab())){
                    error = "Failed to click on 'Monitoring point' tab.";
                    return false;
                }
                if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monitoringPointOption(getData("Monitoring Point")))){
                    error = "Failed to wait for '" + getData("Monitoring Point") + "' option.";
                    return false;
                }
                if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.monitoringPointOption(getData("Monitoring Point")))){
                    error = "Failed to click on '" + getData("Monitoring Point") + "' option.";
                    return false;
                }
                narrator.stepPassedWithScreenShot("Monitoring Point : '" + getData("Monitoring Point") + "'.");
                break;
            default:
                narrator.stepPassedWithScreenShot("Air Quality Type : '" + airQualityType + "'.");
        } 
        
        //Month / Year
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monthTab())){
            error = "Failed to wait for 'Month' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.monthTab())){
            error = "Failed to click on 'Month' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.monthOption(getData("Month")))){
            error = "Failed to wait for '" + getData("Month") + "' option.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.monthOption(getData("Month")))){
            error = "Failed to click on '" + getData("Month") + "' option.";
            return false;
        }
        
        //Year
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.yearTab())){
            error = "Failed to wait for 'Year' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.yearTab())){
            error = "Failed to click on 'Year' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.yearOption(getData("Year")))){
            error = "Failed to wait for '" + getData("Year") + "' tab.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.yearOption(getData("Year")))){
            error = "Failed to click on '" + getData("Year") + "' tab.";
            return false;
        }
        
        //Add support document

//        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.uploadLinkbox())){
//            error = "Failed to wait for 'Link box' link.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.uploadLinkbox())){
//            error = "Failed to click on 'Link box' link.";
//            return false;
//        }
//        pause(1000);
//        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");
//        
//        //switch to new window
//        if(!SeleniumDriverInstance.switchToTabOrWindow()){
//            error = "Failed to switch to new window or tab.";
//            return false;
//        }
//        
//        //URL https
//        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.LinkURL())){
//            error = "Failed to wait for 'URL value' field.";
//            return false;
//        }      
//        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.LinkURL(), getData("Document Link") )){
//            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");
//        
//        //Title
//        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.urlTitle())){
//            error = "Failed to wait for 'Url Title' field.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.urlTitle(), getData("Title"))){
//            error = "Failed to enter '" + getData("Title") + "' into 'Url Title' field.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");
//        
//        //Add button
//        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.urlAddButton())){
//            error = "Failed to wait for 'Add' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.urlAddButton())){
//            error = "Failed to click on 'Add' button.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
//        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");
//        
//         //switch to the iframe
////        if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
////            error = "Failed to switch to frame.";
////        }
////        if (!SeleniumDriverInstance.switchToFrameByXpath(IsoMetricsIncidentPageObjects.iframeXpath())) {
////            error = "Failed to switch to frame.";
////        }
//        
//         if (!SeleniumDriverInstance.waitForElementByXpath(IsoMetricsIncidentPageObjects.iframeName())) {
//            error = "Failed to locate to switch to frame.";
//        }
//        
//         if (!SeleniumDriverInstance.swithToFrameByName(IsoMetricsIncidentPageObjects.iframeName()))
//        {
//            error = "Failed to switch to frame ";
//            
//        }
//        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
//
//        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.uploadLinkbox())){
//            error = "Failed to wait for 'Link box' link.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.uploadLinkbox())){
//            error = "Failed to click on 'Link box' link.";
//            return false;
//        }
//        
//        narrator.stepPassedWithScreenShot("Successfully click 'Upload Hyperlink box'.");
//        
//        //switch to new window
//        if(!SeleniumDriverInstance.switchToTabOrWindow()){
//            error = "Failed to switch to new window or tab.";
//            return false;
//        }
//        
//        //URL https
//        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.LinkURL())){
//            error = "Failed to wait for 'URL value' field.";
//            return false;
//        }      
//        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.LinkURL(), getData("Document Link") )){
//            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");
//        
//        //Title
//        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.urlTitle())){
//            error = "Failed to wait for 'Url Title' field.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.urlTitle(), getData("Title"))){
//            error = "Failed to enter '" + getData("Title") + "' into 'Url Title' field.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Title : '" + getData("Title") + "'.");
//        
//        //Add button
//        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.urlAddButton())){
//            error = "Failed to wait for 'Add' button.";
//            return false;
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.urlAddButton())){
//            error = "Failed to click on 'Add' button.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
//        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");
//        
//         //switch to the iframe
//        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//        if (!SeleniumDriverInstance.switchToFrameByXpath(Air_Quality_Monitoring_PageObjects.iframeXpath())) {
//            error = "Failed to switch to frame.";
//        }
//        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");
       
        //Save to continue
        if(!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.saveToContinueButton())){
            error = "Failed to wait for 'Save to continue' button.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.saveToContinueButton())){
            error = "Failed to click on 'Save to continue' button.";
            return false;
        }

        
//        //Saving mask
//        if (SeleniumDriverInstance.waitForElementByXpath(IncidentPageObjects.saveWait(), 2)) {
//            if (!SeleniumDriverInstance.waitForElementPresentByXpath(IncidentPageObjects.saveWait2(), 400)) {
//                error = "Webside too long to load wait reached the time out";
//                return false;
//            }
//        }
        pause(12000);
         narrator.stepPassedWithScreenShot("Successfully saved the record");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.airQualityMonitorRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        narrator.stepPassedWithScreenShot("Measurements and Air Quality Monitoring Findings panels are displayed");
        String retrievePanel1text = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.measurementsPanel()); 
        if (!retrievePanel1text.equalsIgnoreCase("Measurements")) {
                error = "Failed to retrieve Measurements panel text ";
                return false;
            }

        String retrievePanel2text = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.AQMFindingsPanel()); 
        if (!retrievePanel2text.equalsIgnoreCase("Air Quality Monitoring Findings")) {
                error = "Failed to retrieve Air Quality Monitoring Findings panel text ";
                return false;
            }

        
        return true;
    }
}


