/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses;

import KeywordDrivenTestFramework.Testing.TestClasses.Air_Quality_Monitoring_TestClasses.*;
import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.testData;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.Air_Quality_Monitoring_PageObjects;

import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import static org.sikuli.script.RunTime.pause;

/**
 *
 * @author SJonck
 */
@KeywordAnnotation(
        Keyword = "Capture Findings",
        createNewBrowserInstance = false
)

public class FR4_CaptureFindings_MS extends BaseClass
{

    String error = "";
    SikuliDriverUtility sikuliDriverUtility;

    public FR4_CaptureFindings_MS()
    {
        this.sikuliDriverUtility = new SikuliDriverUtility("SikuliImages\\Images\\");
    }

    public TestResult executeTest()
    {
        if (!navigateToCaptureFindings())
        {
            return narrator.testFailed("Failed due - " + error);
        }
        if (!enterDetails())
        {
            return narrator.testFailed("Failed To Capture Findings Due To " + error);
        }
        return narrator.finalizeTest("Successfully Captured Findings");
    }

    public boolean navigateToCaptureFindings()
    {
        //Navigate to Capture Findings
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_panel()))
        {
            error = "Failed to wait for 'Capture Findings' panel.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_panel()))
        {
            error = "Failed to click on 'Capture Findings' panel.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Successfully navigated to 'Capture Findings' tab.");

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_Add()))
        {
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.AQMF_Add()))
        {
            error = "Failed to click on 'Add' button.";
            return false;
        }
        pause(2000);
        narrator.stepPassedWithScreenShot("Successfully clicked Add button.");

        return true;
    }

    //Enter data
    public boolean enterDetails()
    {

        //Finding description
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.AQMF_desc()))
        {
            error = "Failed to wait for 'Description' textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.AQMF_desc(), testData.getData("Description")))
        {
            error = "Failed to enter text '" + testData.getData("Description") + "' into 'Description' textarea.";
            return false;
        }

        narrator.stepPassedWithScreenShot("Entered text into description.");
        //Finding description
          if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Findings_desc()))
        {
            error = "Failed to wait for Description textarea.";
            return false;
        }
        if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.Findings_desc(), testData.getData("Description")))
        {
            error = "Failed to enter text into Description textarea.";
            return false;
        }

   
        
       //Findings Owner dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Findings_owner_dropdown()))
        {
            error = "Failed to wait for Findings Owner dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Findings_owner_dropdown()))
        {
            error = "Failed to click Findings Owner dropdown.";
            return false;
        }
       

        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch()))
        {
            error = "Failed to wait for Measurement taken by text box.";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(Air_Quality_Monitoring_PageObjects.TypeSearch(), getData("Findings Owner")))
        {
            error = "Failed to enter  Findings Owner :" + getData("Findings Owner");
            return false;
        }
         if (!SeleniumDriverInstance.pressEnter_2(Air_Quality_Monitoring_PageObjects.TypeSearch()))
        {
            error = "Failed to press enter";
            return false;
        }
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Findings Owner"))))
        {
            error = "Failed to wait for Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Text2(getData("Findings Owner"))))
        {
            error = "Failed to click Findings Owner drop down option : " + getData("Findings Owner");
            return false;
        }

        //Risk Source dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Findings_risksource_dropdown()))
        {
            error = "Failed to wait for Risk source dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Findings_risksource_dropdown()))
        {
            error = "Failed to click Risk source dropdown.";
            return false;
        }
        //Risk Source select option 1
//        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.FindingsRiskSourceSelectAll()))
//        {
//            error = "Failed to wait  Risk Source dropdown options";          
//            return false;
//        }
//        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.FindingsRiskSourceSelectAll()))
//        {
//            error = "Failed to click Risk Source dropdown option";
//            return false;
//        }
        
         //Findings Classification dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Findings_class_dropdown()))
        {
            error = "Failed to wait for Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Findings_class_dropdown()))
        {
            error = "Failed to click Findings Classification dropdown.";
            return false;
        }
        //Findings Classification select
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.Findings_class_select(testData.getData("Findings Classification"))))
        {
            error = "Failed to wait for '" + testData.getData("Findings Classification") + "' in Findings Classification dropdown.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.Findings_class_select(testData.getData("Findings Classification"))))
        {
            error = "Failed to click '" + testData.getData("Findings Classification") + "' from Findings Classification dropdown.";
            return false;
        }

        
        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.SaveButtonFindings2()))
        {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(Air_Quality_Monitoring_PageObjects.SaveButtonFindings2()))
        {
            error = "Failed to click Save button";
            return false;
        }

        //Check if the record has been Saved
        if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup()))
        {
            error = "Failed to wait for 'Record Saved' popup.";
            return false;
        }

        String saved = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.recordSaved_popup());

        if (saved.equals("Record saved"))
        {
            narrator.stepPassedWithScreenShot("Successfully clicked 'Save' button.");
        } else
        {
            if (!SeleniumDriverInstance.waitForElementByXpath(Air_Quality_Monitoring_PageObjects.failed()))
            {
                error = "Failed to wait for error message.";
                return false;
            }

            String failed = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.failed());

            if (failed.equals("ERROR: Record could not be saved"))
            {
                error = "Failed to save record.";
                return false;
            }
        }
        
         //Getting the action No
            String acionRecord = SeleniumDriverInstance.retrieveTextByXpath(Air_Quality_Monitoring_PageObjects.getActionRecord());
            narrator.stepPassed("Record number :" + acionRecord);

        return true;
    }

}
