/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring;

import KeywordDrivenTestFramework.Testing.TestMarshall;

/**
 *
 * @author MJivan
 */
public class IsometricsPOCPageObjects {

    public static String IsometrixURL() {
        // Use ENUM
   //  return TestMarshall.currentEnvironment.isoMetrixEnvironment;
        return "https://usazu-pr01.isometrix.net/IsoMetrix.UK.V4.Automation/default.aspx";
    }
    
    public static String IsometrixURLDatabase() {
        // Use ENUM
   //  return TestMarshall.databaseEnvironment.isoMetrixEnvironment;
        return "https://usazu-pr01.isometrix.net/IsoMetrix.UK.V4.Automation/default.aspx";
    }
    
    public static String URL(String text) {
        
        return text;
    
    }
    
    
    public static String userProjectXpath(){
        return "//span[@class='user']";
    }

    public static String Username() {
        return "//input[@id='txtUsername']";
    }

    public static String Password() {
        return "//input[@id='txtPassword']";
    }

    public static String LoginBtn() {
        return "//div[@id='btnLoginSubmit']";
    }

    public static String homepage_SolutionBranch_Btn() {
        return"//label[text()='Solutions Branch']";
    }

    public static String homepage_SolutionBranchCode_Btn() {
        return "(//div[text()='Search'])[1]";
    }

    public static String backtohomepage_Btn() {
        return"//div[@class='active form transition visible']//i[@class='back icon arrow-left']";
    }
    
    public static String iframeXpath(){
        return "//iframe[@id='ifrMain']";
    }

    
   public static String iframeName()
    {
        return "ifrMain";
    }

    
    public static String isoSideMenu(){
            return "//i[@class='large sidebar link icon menu']";
    }
    public static String version(){
        return "//footer";
    }
    
    public static String sb_text(){
        return "//div[text()='Drag a column header and drop it here to group by that column']/../div[3]//div[@class='align-left']";
    }

}
