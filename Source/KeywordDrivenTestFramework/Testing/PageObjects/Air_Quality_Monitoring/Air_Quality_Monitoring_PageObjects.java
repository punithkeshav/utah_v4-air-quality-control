/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class Air_Quality_Monitoring_PageObjects extends BaseClass
{

    public static String Text3(String data)
    {
        return "(//li[@title='" + data + "']//a[text()='" + data + "'])";
    }

    public static String businessUnitOption1(String text)
    {
        return "(//div[contains(@class,'transition visible')]//a[text()='" + text + "']/..//i)[1]";
    }

    
   
    public static String businessUnitDropdown()
    {
        return "//div[@id='control_739C430A-AAD9-47C4-80E3-30FE13AB4D33']//li";
    }

    public static String linkToADocument()
    {
        return "//div[@id='control_36C93CF0-9F8F-4661-A131-810C96569C49']//b[@original-title='Link to a document']";
    }

    public static String businessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String LinkURL()
    {
        return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String ButtonConfirm()
    {
        return "//div[@id='btnConfirmYes']";
    }

    public static String waterTransactionRecordNumber_xpath()
    {
        return "(//div[@id='form_F517B4C9-16D1-4734-9DAB-8AEDA8426F53']//div[contains(text(),'- Record #')])[1]";
    }

    public static String linkADoc_buttonxpath()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String iframe()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String linkADoc_button()
    {
        return "//b[@original-title='Link to a document']";
    }

    public static String linkADoc_Add_buttonxpath()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String urlInput_TextAreaxpath()
    {
        return "//input[@id='urlValue']";
    }

    public static String tile_TextAreaxpath()
    {
        return "//input[@id='urlTitle']";
    }

    public static String DeleteButton()
    {
        return "//div[@id='btnDelete_form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']";
    }

    public static String ButtonOK()
    {
        return "//div[contains(@class,'centreTopPopup transition visible')]//div[text()='OK']";
    }

    public static String SearchButton()
    {
        return "//div[@id='btnActApplyFilter']";
    }

    public static String Record(String string)
    {
        return "//span[text()='" + string + "']";
    }

    public static String CloseCurrentModule2()
    {
        return "(//div[contains(@class,'form transition visible active')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String CloseCurrentModule()
    {
        return "(//div[contains(@class,'form active transition visible')]//div[@class='navbar']//i[@class='close icon cross'])[1]";
    }

    public static String RecordFind()
    {

        return "//i[@class='icon edit on paper icon ten six grid-icon-edit grid-icon-edit-active']";
    }

    public static String ContainsTextBox()
    {
        return "//input[@class='txt border']";
    }

    public static String Findings_owner_dropdown()
    {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']//li";
    }

    public static String SaveButtonFindings()
    {
        return "//div[@id='btnSave_form_6F352166-D6BB-4AF3-B90E-6B8A72FCDF7A']";
    }

    public static String SaveButtonFindings2()
    {
        return "//div[@id='btnSave_form_A4BB87E5-A0CA-4A12-A57D-06244726EDAA']";
    }

    public static String FindingsRiskSourceSelectAll()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@original-title='Select all']";
    }

    public static String Findings_owner_select(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[1]";
    }

    public static String Findings_risksource_dropdown()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//li";
    }

    public static String Findings_risksource_select(String option)
    {
        return "(//div[contains(@class,'transition visible')]//li[@title='" + option + "']//i[@class='jstree-icon jstree-ocl'])[1]";
    }

    public static String Findings_risksource_select2(String text)
    {
        return "(//a[text()='" + text + "']/i[1])";
    }

    public static String Findings_risksource_arrow()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']//b[@class='select3-down drop_click']";
    }

    public static String Findings_class_dropdown()
    {
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']//li";
    }

    public static String Findings_class_select(String text)
    {
        return "(//a[contains(text(),'" + text + "')])[1]";
    }

    public static String Findings_risk_dropdown()
    {
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']//li";
    }

    public static String Findings_risk_select(String text)
    {
        return "(//a[text()='" + text + "'])[1]";
    }

    public static String Findings_stc()
    {
        return "(//div[text()='Save to continue'])[2]";
    }

    public static String findingsProcessFlowEditText()
    {
        return "(//div[text()='Edit phase'])[3]";
    }

    public static String uploadDocument()
    {
        return "//div[@id='control_C440202B-AFE6-4EB9-B180-57BBF25C57D6']//b[@original-title='Upload a document']";
    }

    public static String Findings_desc()
    {
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }

    public static String failed()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record has no changes to save')]";
    }

    public static String recordSaved_popup()
    {
        return "//div[contains(@class,'transition visible')]//div[@id='txtHeader'][contains(text(),'Record saved')]";
    }

    public static String linkADoc_Add_button()
    {
        return "//div[@id='btnConfirmYes']//div[text()='Add']";
    }

    public static String MaskBlock()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String MaskNone()
    {
        return "//div[@class='ui inverted dimmer']";
    }

    public static String BusinessUnitDropDownOption1()
    {
        return "(//a[text()='Base Metals']/..//i)[1]";
    }

    public static String BusinessUnitDropDownOption2(String text)
    {
        return "((//a[text()='" + text + "'])/..//i)[1]";
    }

    public static String Text2(String text)
    {
        return "//div[contains(@class,'transition visible')]//a[text()='" + text + "']";

    }

    public static String TypeSearch()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type and enter to search']";
    }

    public static String TypeSearch2()
    {
        return "//div[contains(@class,'transition visible')]//input[@placeholder='Type to search']";
    }

    public static String BusinessUnitexpandButton()
    {
        return "//div[contains(@class,'transition visible')]//i[@class='jstree-icon jstree-ocl']";
    }

    public static String Text(String text)
    {
        return "//a[text()='" + text + "']";
        //return "//div[contains(@class,'transition visible')]//a[contains(text(),'"+text+"']";
    }
    
    
     public static String Text4(String text)
    {
       
       return "//a[contains(text(),'"+text+"')]";
    }

    public static String iframeName()
    {
        return "ifrMain";
    }

    public static String iframeXpath()
    {
        return "//iframe[@id='ifrMain']";
    }

    public static String EnvironmentalSustainabilityTab()
    {
        return "//label[text()='Environmental Sustainability']";
    }

    public static String saveWait()
    {
        return "//div[@class='ui inverted dimmer active']";
    }

    public static String saveWait2()
    {
        return "//div[@class='ui inverted dimmer']/div[text()='Saving...']";
    }

    public static String businessUnitexpand(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String tailingManagementTab()
    {
        return "(//label[contains(text(),'Tailings Management')])[1]";
    }

    public static String tailingManagementMonitoringTab()
    {
        return "//label[contains(text(),'Tailings Management Monitoring')]";
    }

    public static String airQualityMonitoringTab()
    {
        return "//label[contains(text(),'Air Quality Monitoring')]";
    }

    public static String BusinessUnitDropDownOption(String text)
    {
        return "(//div[contains(@class,'transition visible')]//ul[@class='jstree-children']//a[text()='" + text + "']/..//i)[1]";
    }

    public static String addButton()
    {
        return "//div[@id='btnActAddNew']";
    }

    public static String businessUnitTab()
    {
        return "//div[@id='control_739C430A-AAD9-47C4-80E3-30FE13AB4D33']";
    }

    public static String businessUnitSelect(String text)
    {
        return "//div[@id='divForms']/div[4]//a[contains(text(),'" + text + "')]";
    }

    public static String airQualityType()
    {
        return "//div[@id='control_164E9ECF-98B7-44BE-810E-9E6D002BE0EB']";
    }

    public static String airQualityTypeSelect(String text)
    {
        return "//div[@id='divForms']/div[6]//a[contains(text(),'" + text + "')]";
    }

    public static String monthTab()
    {
        return "//div[@id='control_4E1D4CE9-349A-491A-8D18-EFD53A2076C7']";
    }

    public static String monthOption(String text)
    {
        return "//div[@id='divForms']/div[7]//a[contains(text(),'" + text + "')]";
    }

    public static String yearTab()
    {
        return "//div[@id='control_228918B1-E384-4DE9-9910-2D881B26DA23']";
    }

    public static String yearOption(String text)
    {
        return "//div[@id='divForms']/div[8]//a[contains(text(),'" + text + "')]";
    }

    public static String monitoringPointTab()
    {
        return "//div[@id='control_E96E3D7B-B465-4EEE-B615-F90459343BE6']";
    }

    public static String monitoringPointOption(String text)
    {
        return "(//div[@id='divForms']//a[contains(text(),'" + text + "')])[1]";
    }

    public static String uploadLinkbox()
    {
        return "//div[@class='linkbox-options']//b[@class='linkbox-link']";
    }

    public static String urlTitle()
    {
        return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
        return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String saveToContinueButton()
    {
        return "//div[@id='control_D4D50C09-26C5-410D-9549-2CC4BE86A134']";
    }

    public static String saveSupportingDocuments()
    {
        return "//div[@id='control_F1395E4D-7A99-4032-A127-20F4D7E31DE2']";
    }

    public static String airQualityMonitorRecordNumber_xpath()
    {
        return "(//div[@id='form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']//div[contains(text(),'- Record #')])[1]";
    }

    public static String emissionMeasureRecordNumber_xpath()
    {
        return "(//div[@id='form_BCE8C74D-00E6-4B6A-AAEF-C92A9F7FDC22']//div[contains(text(),'- Record #')])[1]";
    }

    public static String dustMeasureRecordNumber_xpath()
    {
        return "(//div[@id='form_F8D39CA4-85E3-4C8D-93BD-8B0F9981CF85']//div[contains(text(),'- Record #')])[1]";
    }

    public static String findingsRecordNumber_xpath()
    {
        return "(//div[@id='form_A4BB87E5-A0CA-4A12-A57D-06244726EDAA']//div[contains(text(),'- Record #')])[1]";
    }

    public static String AQMFindingsPanel()
    {
        return "//div[@id='control_F0FAA022-87BA-48E1-9A52-D5856F210E3E']";
    }

    public static String parameterReadingsPanel()
    {
        return "//div[@id='control_3CB28695-7F32-4A0B-9B5E-A7399E51A9B2']";
    }

    public static String measurementsPanel()
    {
        return "//div[@id='control_B85384C4-EF31-486E-BAB1-D66480457717']";
    }

    public static String processFlow2()
    {
        return "//div[@id='btnProcessFlow_form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']";
    }

    //measurements Xpaths
    public static String measurementTab()
    {
        return "//div[@id='control_B85384C4-EF31-486E-BAB1-D66480457717']";
    }

    public static String measurement()
    {
        return "//div[@id='control_B85384C4-EF31-486E-BAB1-D66480457717']//span[contains(text(),'Measurements')]";
    }

    public static String measurementsAddButton()
    {
        return "//div[@id='control_D16ADEB8-A521-42B2-B1FA-DAB4B90D86AC']//div[text()='Add']";
    }

    public static String measurementProcessFlow()
    {
        return "//div[@id='btnProcessFlow_form_F8D39CA4-85E3-4C8D-93BD-8B0F9981CF85']";
    }

    public static String measurementMonitoringTab()
    {
        return "//div[@id='control_EDDF8770-EC82-443B-9004-C993CD9A76B4']//li";
    }

    public static String measurementMonitoringOption(String text)
    {
        return "//a[text()='" + text + "']/../i";
    }

    public static String measurementMonitoringOption2(String data)
    {
        return "(//a[contains(text(),'" + data + "')])[1]";
    }

    public static String measurementFieldTab()
    {
        return "//div[@id='control_0F289C2C-8DDB-4551-A052-E90F299940E2']";
    }

    public static String measurementField()
    {
        return "//div[@id='control_F9C2E750-CC2E-43F6-A5A4-D3084D4C9E09']//input";
    }

    public static String unitTab()
    {
        return "//div[@id='control_C4DA1DA7-E7AE-4597-875B-7C7A043AB15F']//li";
    }

    public static String unitOption(String data)
    {
        return "//a[contains(text(),'" + data + "')]";
    }

    public static String measurementSaveButton()
    {
        return "//div[@id='btnSave_form_F8D39CA4-85E3-4C8D-93BD-8B0F9981CF85']/div[contains(text(),'Save')]";
    }

    public static String CEM_Parameter_panel()
    {
        return "//div[@id='control_3CB28695-7F32-4A0B-9B5E-A7399E51A9B2']//span[text()='Parameter Readings']";
    }

    public static String CEM_Add()
    {
        return "//div[@id='control_3CB28695-7F32-4A0B-9B5E-A7399E51A9B2']//div[text()='Add']";
    }

    public static String CEM_Parameters_dropdown()
    {
        return "//div[@id='control_F11D69C1-F470-4A98-AA57-C7DA50E56DA4']//ul";
    }

    public static String CEM_Parameters_select(String data)
    {
        return "(//div[@id='divForms']//a[contains(text(),'" + data + "')])[1]";
    }

    public static String MeasurementTextBox()
    {
        return "//div[@id='control_A519A7A7-3788-4E44-A761-AB93971C4477']//input[@type='number']";
    }

    public static String SampleDate()
    {
        return "//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input";
    }

    public static String CEM_stakenby()
    {
        return "//div[@id='control_293D62D6-7F8B-4F4C-935C-8D9E57CB23B0']/div/div//input";
    }

    public static String CommentsTextBox()
    {
        return "//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']/div/div//textarea";
    }

    public static String CEM_Save()
    {
        return "//div[@id='btnSave_form_BCE8C74D-00E6-4B6A-AAEF-C92A9F7FDC22']";
    }

    public static String AQMF_panel()
    {
        return "//div[@id='control_F0FAA022-87BA-48E1-9A52-D5856F210E3E']//span[text()='Air Quality Monitoring Findings']";
    }

    public static String AQMF_Add()
    {
        return "//div[@id='control_F0FAA022-87BA-48E1-9A52-D5856F210E3E']//div[text()='Add']";
    }

    public static String AQMF_processflow()
    {
        return "//div[@id='btnProcessFlow_form_A4BB87E5-A0CA-4A12-A57D-06244726EDAA']";
    }

    public static String AQMF_desc()
    {
        return "//div[@id='control_40ECC722-B08B-48F3-9906-3CFCE527C5CD']//textarea";
    }

    public static String AQMF_owner_dropdown()
    {
        return "//div[@id='control_434D9128-5EBC-4E25-9836-72A2C4451733']";
    }

    public static String AQMF_owner_select(String text)
    {
        return "//a[contains(text(),'" + text + "')]";
    }

    public static String AQMF_risksource_dropdown()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']";
    }

    public static String AQMF_risksource_select(String text)
    {
        return "//a[text()='" + text + "']/../i";
    }

    public static String AQMF_risksource_select2(String text1, String text2)
    {
        return "//a[text()='" + text1 + "']/..//a[text()='" + text2 + "']/i[1]";
    }

    public static String AQMF_class_dropdown()
    {
        return "//div[@id='control_7689E71C-DC7E-41C1-9124-C6312596E956']";
    }

    public static String AQMF_class_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String AQMF_risk_dropdown()
    {
        return "//div[@id='control_1D7F3DE4-F9C4-4626-80C9-0E600A0A8FF8']";
    }

    public static String AQMF_risk_select(String text)
    {
        return "//a[text()='" + text + "']";
    }

    public static String AQMF_arrow()
    {
        return "//div[@id='control_E2CBB706-26AB-4373-813A-05D4860FC38F']/div/a/span[2]/b[1]";
    }

    public static String AQMF_save()
    {
        return "//div[@id='control_E0B5417B-8F24-4E1D-A4DB-8C68192F6F7B']//div[text()='Save to continue']";
    }

    public static String AQMF_savewait()
    {
        return "//div[text()='Air Quality Findings Actions']";
    }

    public static String expandGlobalCompany()
    {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'Global Company')]//..//i)[1]";
    }

    public static String expandSouthAfrica()
    {
        return "(//div[@id='divForms']/div[4]//a[contains(text(),'South Africa')]//..//i)[1]";
    }

    public static String CEM_processFlow()
    {
        return "//div[@id='btnProcessFlow_form_BCE8C74D-00E6-4B6A-AAEF-C92A9F7FDC22']";
    }

    public static String CEM_uploadLinkbox()
    {
        return "//div[@id='control_F60F4398-06EE-403F-891A-BFEA6314167E']//b[@class='linkbox-link']";
    }

    public static String SampleTakenByDropDown()
    {
        return "//div[@id='control_A68322E0-4BC2-4A8C-A57D-5C6381E882B3']//li";
    }

    public static String saveButton()
    {
        return "//div[@id='btnSave_form_74DBD0CA-CB81-485B-AFD9-8F5F59BEFD9D']";
    }

    public static String getActionRecord()
    {
        return "//div[@class='record']";
    }

    public static String LinkToSite()
    {
        return "//div[@id='control_198375A9-1B33-46D1-A93F-180F33F21D25']";
    }

    public static String LinkToSiteDropDown()
    {
        return "//div[@id='control_3544E0CE-09D7-408E-8CB9-EC387D9B3675']//li";
    }

    public static String LinkToProjects()
    {
        return "//div[@id='control_3018FA38-B287-444A-A4F0-E4CD583F047E']";
    }

    public static String LinkToProjectsDropDown()
    {
        return "//div[@id='control_44FA56D6-420C-4B96-A208-AE39A0424D3C']//li";
    }

    public static String SavingMask()
    {
        return "//div[contains(@class,'form active transition visible')]//div[text()='Saving...']";
    }

    public static String Parameters_dropdown()
    {
        return "//div[@id='control_7772C02F-A0D6-4722-B46B-F56F365AE1D8']";
    }

    public static String Unit_dropdown()
    {
        return "//div[@id='control_262495D5-3D67-461A-AE55-2BB7CE6BD497']";
    }

    public static String BusinessUnitDropDownOption_2(String data)
    {
        return "(//a[text()='" + data + "'])";
    }

    public static String Parameters_Table()
    {
        return "//div[@id='control_25F6C1D4-DFEC-4B6F-BA63-DF23DD81C784']//input[@type='number']";
    }

    public static String Measurement_Text_Box(int i)
    {
        return "(//div[@id='control_25F6C1D4-DFEC-4B6F-BA63-DF23DD81C784']//input[@type='number'])[" + i + "]";
    }

    public static String CommentsTextBox(int i)
    {
        return "(//div[@id='control_25F6C1D4-DFEC-4B6F-BA63-DF23DD81C784']//textarea[@class='txt inline-multiline'])[" + i + "]";
    }

    public static String SampleDate(int i)
    {
        return "(//div[@id='control_25F6C1D4-DFEC-4B6F-BA63-DF23DD81C784']//span[@class='k-picker-wrap k-state-default'])[" + i + "]//input";
    }

    public static String Unit_dropdown(int i)
    {
        return "(//div[@id='control_25F6C1D4-DFEC-4B6F-BA63-DF23DD81C784']//div[@name='ddl13'])[" + i + "]";
    }

    public static String Parameters_dropdown(int i)
    {
        return "(//div[@id='control_7772C02F-A0D6-4722-B46B-F56F365AE1D8'])[" + i + "]";
    }

    public static String EmissionMeasurementsDeleteButtons()
    {
        return "(//i[@class='icon bin icon ten six grid-icon-delete'])";
    }

    public static String measurementTextbox(int i)
    {
        return "(//div[@id='control_A519A7A7-3788-4E44-A761-AB93971C4477']//input[@type='number'])['" + i + "']";
    }

    public static String sampleDateXpath(int i)
    {
        return "(//div[@id='control_D003AA75-8B41-419A-A615-2044F720C3E6']//input)[" + i + "]";
    }

    public static String commentsTextbox(int i)
    {
        return "(//div[@id='control_CE92607E-3CB8-4B9B-90D5-EA4C22CC17EA']//textarea)[" + i + "]";
    }

    public static String UnitDropdown(int i)
    {
        return "(//div[@id='control_262495D5-3D67-461A-AE55-2BB7CE6BD497']//ul)['" + i + "']";
    }

    public static String measurementTextbox_1()
    {
        return "//div[@class='textbox textbox_15c73398']//input";
    }

    public static String measurementTextbox_2()
    {
        return "//div[@class='textbox textbox_3718f151']//input";
    }

    public static String TeamNameDropDown()
    {
       return "//div[@id='control_295CDD0A-54AB-429C-B4C4-CA62DAC55F3C']//ul";
    }

    public static String ParameterReadingsDropdown()
    {
        return"//div[@id='control_7772C02F-A0D6-4722-B46B-F56F365AE1D8']";
    }

    public static String Deletebutons()
    {
       return "//i[@class='icon bin icon ten six grid-icon-delete']";
    }

    public static String ParameterDropdown()
    {
       return "//div[@id='control_7772C02F-A0D6-4722-B46B-F56F365AE1D8']//ul";
    }
}
