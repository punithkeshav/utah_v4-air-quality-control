/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author RNagel
 */
public class S4_Air_Quality_Monitoring_TestSuite extends BaseClass {

    static TestMarshall instance;

    public S4_Air_Quality_Monitoring_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.MTN;

        //*******************************************
    }

    //Ch05
     @Test
    public void Air_Quality_Monitoring_Ch05() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Air_Quality_Monitoring-Regression Ch05.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
        
    }
    
    
    //S4_Air Quality Monitoring_SAPROD
    @Test
    public void Air_Quality_Monitoring_SA_PROD() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Air_Quality_Monitoring-Regression SA_PROD.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
        
    }    
      @Test
    public void Air_Quality_Monitoring_tranning() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Air_Quality_Monitoring-Regression training.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
        
    } 
    
    
    @Test
    public void Air_Quality_Monitoring() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\Air_Quality_Monitoring-Regression.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
        
    }    
    //FR1-Capture_Air_Quality_Monitoring - Main_Scenario
    @Test
    public void FR1_CaptureAirQualityMonitoring_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capture_Air_Quality_Monitoring - Main_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_CaptureAirQualityMonitoring_AlternateScenario1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capture_Air_Quality_Monitoring - Alternate_Scenario1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void FR1_CaptureAirQualityMonitoring_AlternateScenario2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capture_Air_Quality_Monitoring - Alternate_Scenario2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
     @Test
    public void FR1_CaptureAirQualityMonitoring_AlternateScenario3() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capture_Air_Quality_Monitoring - Alternate_Scenario3.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR1_CaptureAirQualityMonitoring_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR1-Capture_Air_Quality_Monitoring - Optional_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2-Capture_Emissions_Measurement - MainScenario
    @Test
    public void FR2_CaptureEmissionsMeasurement_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR2-Capture_Emissions_Measurement - MainScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR2_CaptureEmissionsMeasurement_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR2-Capture_Emissions_Measurement - OptionalScenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
        
    //FR3-Capture_Dust_Measurements - Main_Scenario
    @Test
    public void FR3_CaptureDustMeasurements_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR3-Capture_Dust_Measurements - Main_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
   
    //FR4-Capture_Findings - Main_Scenario
    @Test
    public void FR4_CaptureFindings_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR4-Capture_Findings - Main_Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      @Test
    public void FR5_Edit_Air_Quality_Monitoring_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR5-Edit Air Quality Monitoring.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
      @Test
    public void FR6_Delete_Air_Quality_Monitoring_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V4 - Test Pack");
        instance = new TestMarshall("TestPacks\\FR6-Delete Air Quality Monitoring.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
